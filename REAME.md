How to start

Pre-conditions:
* Check **Node.js** is installed on your PC.
* Check browsers are installed on your PC.

1. Install the npm packages using comand: **npm install**
2. Execute the following command to start test run: **npm run test-chrome**

Tests will be run for Chrome browser. To run them for Firefox, execure the command: **npm run test-ff**
