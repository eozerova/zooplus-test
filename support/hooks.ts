import { After, AfterAll, Before, BeforeAll, Status, setDefaultTimeout } from '@cucumber/cucumber';
import config from 'config';
import driver from '../base/factory';
import { GeneralPage } from '../models/pages/general.page';
import ShareData from '../base/memory';
import * as crypto from 'node:crypto'

setDefaultTimeout(60 * 1000);

BeforeAll({ timeout: 10000 }, async function () {
    await driver.manage().setTimeouts({ implicit: 0, pageLoad: 6000, script: 7000 });
});

Before({ timeout: 10000 }, async function () {
    ShareData.clear();
    const general: GeneralPage = new GeneralPage();

    await driver.manage().deleteAllCookies();

    // open app
    await driver.get(config.get("baseUrl"));

    await driver.executeScript('window.localStorage.clear()');
    await driver.executeScript('window.sessionStorage.clear()');

    // manage cookies
    await driver.manage().deleteAllCookies();
    await general.acceptAllCookies();

    // set sid cookies
    await driver.manage().addCookie({ name: 'sid', value: `elena-ozerova-test-${crypto.randomUUID().slice(0, 10)}` });
});

After(async function (testCase) {
    await driver.executeScript('window.localStorage.clear()');
    await driver.executeScript('window.sessionStorage.clear()');
    await driver.manage().deleteAllCookies();
    if (testCase.result.status === Status.FAILED) {
        const screenshot = await driver.takeScreenshot()
        this.attach(screenshot, { mediaType: 'base64:image/png' })
    }

    ShareData.clear();
});

AfterAll(async function () {
    await driver.close();
})
