
export function moneyToNumber(value: string) {
    return +value.replace("€", "").trim();
}

export function numberToMoney(value: number) {
    return `€${value.toFixed(2)}`;
}