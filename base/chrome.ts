const webdriver = require('selenium-webdriver');

export function getChromeDriver() {
    const capabilityName = 'goog:chromeOptions';
    const browserOptions = {
        'args': [
            'window-size=1920,1080'
        ]
    };

    let browserCapabilities = webdriver.Capabilities.chrome();
    browserCapabilities = browserCapabilities.set(capabilityName, browserOptions);

    const driver = new webdriver.Builder().withCapabilities(browserCapabilities).build()
    return driver;
}
