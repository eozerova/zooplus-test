import { WebDriver } from "selenium-webdriver";
import { getChromeDriver } from './chrome';
import { getFirefoxDriver } from './firefox';
import config from 'config';

class DriverFactory {
    private browser: string;

    constructor(browserName) {
        this.browser = browserName;
    }

    getDriver() {
        let driver: WebDriver;
        switch (this.browser) {
            case browsers.CHROME:
                driver = getChromeDriver();
                break;
            case browsers.FIREFOX:
                driver = getFirefoxDriver();
                break;

            default:
                throw new Error(`Browser “${this.browser}” is not available`);
        }
        return driver;
    }
}

const browsers = {
    CHROME: 'chrome',
    FIREFOX: 'firefox'
}

const driver = new DriverFactory(config.get("browserName")).getDriver();
export default driver;