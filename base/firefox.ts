const webdriver = require('selenium-webdriver');

export function getFirefoxDriver() {
    const capabilityName = 'moz:firefoxOptions';
    const browserOptions = {
        'args': [
            '--width=1920',
            '--height=1080'
        ]
    };
    
    let browserCapabilities = webdriver.Capabilities.firefox();
    browserCapabilities = browserCapabilities.set(capabilityName, browserOptions);

    const driver = new webdriver.Builder().withCapabilities(browserCapabilities).build();
    return driver;
}
