class ShareData {
    private memory: Map<string, any>;

    constructor() {
        this.memory = new Map();
    }

    save<T>(key: string, value: T): void {
        if (this.memory.has(key)) {
            throw new Error(
                `You tried to override "${key}" property. This is not allowed`)
        }
        this.memory.set(key, value)
    }

    get<T>(key: string): T {
        if (this.memory.has(key)) {
            return this.memory.get(key)
        }
        throw new Error(`You tried to access ${key} property, but it does not exist.`)
    }

    clear(): void {
        this.memory.clear();
    }
}

export default new ShareData();