Feature: Cart

  Scenario: User can add products from catalog
    Given User opens "<catalog_for_cats>"
    And Adds the product "<product_id>" with <expected_quantity> quantity through catalog
    When User goes to Cart
    Then User should see the added product "<product_id>" with <expected_quantity> quantity
    And Total and subtotal should be correct including shipping fees <shipping_fees>

    Examples: 
      | catalog_for_cats    | product_id | expected_quantity | shipping_fees |
      | /shop/cats/cat_toys |    1583173 |                 3 |          4.99 |

  Scenario: User is allowed to add products from catalog and from recommendations
    Given User opens "<catalog_for_cats>"
    And Adds the product "<product_id>" with <expected_quantity> quantity through catalog
    When User goes to Cart
    And Adds the product from recommendation
    Then Products should be added
    And Total and subtotal should include both products and shipping fees <shipping_fees>

    Examples: 
      | catalog_for_cats    | product_id | expected_quantity | shipping_fees |
      | /shop/cats/cat_toys |    1583173 |                 2 |         4.99 |

  Scenario: User can delete the last product from the Cart
    Given User opens "<catalog_for_cats>"
    And Adds the product "<product_id>" with <expected_quantity> quantity through catalog
    When User goes to Cart
    And Removes "<product_id>" product
    Then The "<message>" message should be displayed

    Examples: 
      | catalog_for_cats    | product_id | expected_quantity | message                       |
      | /shop/cats/cat_toys |    1583173 |                 1 | Your shopping basket is empty |

  Scenario: User can manage added products in the Cart
    Given User opens "<catalog_for_cats>"
    And Adds several products "<product_ids>" with <expected_quantity> quantity through catalog
    When User goes to Cart
    And Increments by one the most cheap product
    Then Cart subtotal and total amounts should be updated
    When User removes the most expensive product
    Then Cart subtotal and total amounts should be updated

    Examples: 
      | catalog_for_cats    | product_ids            | expected_quantity |
      | /shop/cats/cat_toys | 1584464,1855415,127454 |                 1 |

  Scenario: User can change shipping country
    Given User opens "<catalog_for_cats>"
    And Adds the product "<product_id>" with <expected_quantity> quantity through catalog
    When User goes to Cart
    And Sets shipping country "<country>" and postcode <postcode>
    Then Shipping fees should be changed to <expected_shippping_fees>
    And Country "<country>" and postcode <postcode> should be displayed
    And Total amount should be updated by new shipping fees <expected_shippping_fees>

    Examples: 
      | catalog_for_cats    | product_id | expected_quantity | country  | postcode | expected_shippping_fees |
      | /shop/cats/cat_toys |    1583173 |                 3 | Portugal |     5000 |                    6.99 |

  Scenario: User should not be allowed to place the order if it does not match minimal order
    Given User opens "<catalog_for_cats>"
    And Adds the product "<product_id>" with <expected_quantity> quantity through catalog
    When User goes to Cart
    Then "Proceed to checkout" button should be disabled
    And Warning message "<message>" should be displayed

    Examples: 
      | catalog_for_cats    | product_id | expected_quantity | message                                                                 |
      | /shop/cats/cat_toys |     127454 |                 1 | Please note: the minimum order value is €19.00 (without shipping costs) |
