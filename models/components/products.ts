import { By, WebDriver, WebElement, until } from 'selenium-webdriver';
import driver from '../../base/factory';
import { ICatalogProductInfo, IProduct } from '../interfaces/product_interface';

export class Products implements IProduct {
    private driver: WebDriver;

    private PRODUCT_ROW_BY_ID = (id: string) => `//*[contains(@class,"productRow") and .//div[@data-shop-identifier="${id}"]]`;
    private OPTIONS_BY_PRODUCT_ID = (productId: string) => `(//div[@data-shop-identifier="${productId}"]/div[@data-zta="product-variant"])[1]`;

    private OPTION_CURRENT_PRICE = (productId: string) => By.xpath(`${this.OPTIONS_BY_PRODUCT_ID(productId)}//span[contains(@class,"z-price__amount")]`);
    private PRODUCT_NAME = (productId: string) => By.xpath(`${this.PRODUCT_ROW_BY_ID(productId)}//a[@data-zta="product-link"]`);
    private PRODUCT_QUANTITY_INPUT = (productId: string) => By.xpath(`${this.OPTIONS_BY_PRODUCT_ID(productId)}//input[@aria-label="Number of items"]`);
    private ADD_TO_CART_BUTTON = (productId: string) => By.xpath(`${this.OPTIONS_BY_PRODUCT_ID(productId)}//button[@data-zta="add-to-cart"]`);
    private CONFIRMATTION_ICON = (productId: string) => By.xpath(`//div[@data-shop-identifier="${productId}"]//*[@class="ProductListItemVariant-module_addToCartFeedbackIcon__q-CBy"]`);

    constructor() {
        this.driver = driver;
    }

    // Product box

    async addProductToBasket(productId: string, quantity = 1): Promise<ICatalogProductInfo> {
        const catalogProductInfo: ICatalogProductInfo = {
            price: await this.getPrice(productId),
            name: await this.getName(productId),
            quantity,
        };

        if (quantity > 1) {
            await this.setOptionQuantity(productId, quantity);
        }

        await this.addToCart(productId);
        return catalogProductInfo;
    }

    async getPrice(productId: string) {
        return await this.driver.findElement(this.OPTION_CURRENT_PRICE(productId)).getText();
    }

    async getName(productId: string) {
        return (await this.driver.findElement(this.PRODUCT_NAME(productId)).getText()).trim();
    }

    async addToCart(productId: string) {
        do {
            await this.driver.findElement(this.ADD_TO_CART_BUTTON(productId)).click();
        } while (!(await this.driver.wait(until.elementLocated(this.CONFIRMATTION_ICON(productId)), 6000).isDisplayed));

        await this.driver.sleep(2000);
    }

    private async setOptionQuantity(productId: string, quantity: number) {
        await this.driver.findElement(this.PRODUCT_QUANTITY_INPUT(productId)).clear();
        await this.driver.findElement(this.PRODUCT_QUANTITY_INPUT(productId)).sendKeys(quantity);
    }
}




