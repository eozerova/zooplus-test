import { By, WebDriver, WebElement, until } from "selenium-webdriver";
import driver from "../../base/factory";
import { ICatalogProductInfo, IProduct } from "../interfaces/product_interface";

export class Recommendations implements IProduct {
    private driver: WebDriver;

    private PRODUCT_NAME = By.xpath('//div[@data-testid="recommendation-item"]/a');
    private PRODUCT_BY_NAME = (productName: string) => `//div[@data-testid="recommendation-item" and ./a[@id="${productName}"]]`;

    private PRODUCT_PRICE = (productName: string) => By.xpath(`${this.PRODUCT_BY_NAME(productName)}//span[contains(@class,"z-price__amount")]`);
    private ADD_TO_CART_BUTTON = (productName: string) => By.xpath(`${this.PRODUCT_BY_NAME(productName)}//button[contains(@class,"recommendation-item-module_cartBtn")]`);

    constructor() {
        this.driver = driver;
    }

    async addProductToBasket(): Promise<ICatalogProductInfo> {
        await driver.sleep(3000);
        const name = (await this.driver.wait(until.elementLocated(this.PRODUCT_NAME), 5000).getAttribute("id")).trim();
        let catalogProductInfo: ICatalogProductInfo = {
            name,
            price: (await this.getPrice(name)).replace("Now","").replace("Our Price","").trim(),
            quantity: 1
        }

        await this.addToCart(name);
        return catalogProductInfo;
    }

    async getPrice(productName: string) {
        return await this.driver.wait(until.elementLocated(this.PRODUCT_PRICE(productName)), 5000).getText();
    }

    async getName() {
        return await this.driver.wait(until.elementLocated(this.PRODUCT_NAME), 5000).getAttribute("id");
    }

    async addToCart(productName: string) {
        await this.driver.wait(until.elementLocated(this.ADD_TO_CART_BUTTON(productName)), 5000).click();
    }
}