import { By, WebDriver, WebElement, until } from 'selenium-webdriver';
import driver from '../../base/factory';

export class GeneralPage {
    private driver: WebDriver;
    private ACCEPT_COOKIES_BTN = By.xpath('//button[@id="onetrust-accept-btn-handler"]');
    private SHOPPING_BASKET = By.xpath('//a[@href="/checkout/overview"]');

    constructor() {
        this.driver = driver;
    }

    async acceptAllCookies() {
      await this.driver.wait(until.elementLocated(this.ACCEPT_COOKIES_BTN), 10000).click();
    }

    async goToBasket() {
        await this.driver.wait(until.elementLocated(this.SHOPPING_BASKET), 5000).click();
    }
}