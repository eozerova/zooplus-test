import { By, WebDriver, WebElement, until } from 'selenium-webdriver';
import driver from '../../base/factory';
import { Recommendations } from '../components/recommendations';
import { ICartProductInfo } from '../interfaces/product_interface';
import { moneyToNumber } from '../../support/utils';

export class CartPage {
    private driver: WebDriver;
    public recommendations = new Recommendations();
    private CONTINUE_SHOPPING_BTN = By.xpath('//button[@data-zta="continueShoppingBtn"]');
    private EMPTY_PAGE_MESSAGE_H1 = By.xpath('//section[@data-testid="checkout-empty-cart"]//h1');
    private PROCEED_CHECKOUT_BTN = By.xpath('//button[@data-zta="gotoPreviewBottom"]');
    private ALLERT_TEXT = By.xpath('//div[@data-zta="alertText"]/p');

    // Articles by product

    private ARTICLE_BY_PRODUCT_ID = (productId: string) => `//article[.//a[contains(@href,"/${productId}")] and @data-zta="standard_article"]`;
    private ARTICLE_BY_PRODUCT_NAME = (productName: string) => `//*[@data-zta="standard_article" and .//a[@data-zta="productName" and text()="${productName}"]]`;
    private ARTICLE_NAME_BY_ID = (productId: string) => By.xpath(`${this.ARTICLE_BY_PRODUCT_ID(productId)}//a[@data-zta="productName"]`);
    private ARTICLE_PRICE_BY_ID = (productId: string) => By.xpath(`${this.ARTICLE_BY_PRODUCT_ID(productId)}//*[@data-zta="articlePrice"]//span[contains(@class,"z-price__amount")]`);
    private ARTICLE_QUANTITY_BY_ID = (productId: string) => By.xpath(`${this.ARTICLE_BY_PRODUCT_ID(productId)}//*[@data-zta="quantityStepperInput"]`);
    private ARTICLE_SUBTOTAL_BY_ID = (productId: string) => By.xpath(`${this.ARTICLE_BY_PRODUCT_ID(productId)}//*[@data-zta="articleQuantitySubtotal"]//span`);
    private ARTICLE_PRICE_BY_NAME = (productName: string) => By.xpath(`${this.ARTICLE_BY_PRODUCT_NAME(productName)}//*[@data-zta="articlePrice"]//span[contains(@class,"z-price__amount")]`);
    private ARTICLE_QUANTITY_BY_NAME = (productName: string) => By.xpath(`${this.ARTICLE_BY_PRODUCT_NAME(productName)}//*[@data-zta="quantityStepperInput"]`);
    private ARTICLE_SUBTOTAL_BY_NAME = (productName: string) => By.xpath(`${this.ARTICLE_BY_PRODUCT_NAME(productName)}//*[@data-zta="articleQuantitySubtotal"]//span`);

    // Articles

    private ARTICLE_NAMES = By.xpath('//*[@data-zta="standard_article"]//a[@data-zta="productName"]');

    // Cart
    private CART_SUBTOTAL = By.xpath('//*[@data-zta="overviewSubTotalValue"]');
    private CART_TOTAL = By.xpath('//*[@data-zta="total__price__value"]');

    private SHIPPING_COUNTRY_NAME = By.xpath('//a[@data-zta="shippingCountryName"]');
    private SHIPPING_DATA_POPUP = By.xpath('//div[@data-zta="bpaPopoverTemplate"]');
    private POSTCODE_INPUT = By.xpath('//div[@data-zta="shippingCostZipcode"]/input');
    private SHIPPING_COUNTRY_DROPDOWN = By.xpath('//button[@data-zta="dropdownMenuTriggerButton"]');
    private SHIPPING_COUNTRY_OPTIONS = (country: string) => By.xpath(`//div[@data-zta="dropdownMenuMenu"]//button[./li[@data-label="${country}"]]`);
    private UPDATE_SHIPPING_FEES = By.xpath('//button[@data-zta="shippingCostPopoverAction"]');

    private SHIPPING_COST = By.xpath('//p[@data-zta="shippingCostValueOverview"]');

    constructor() {
        this.driver = driver;
    }

    // Common

    async continueShopping() {
        await this.driver.findElement(this.CONTINUE_SHOPPING_BTN).click();
    }

    async getEmptyPageMessage() {
        return await this.driver.wait(until.elementLocated(this.EMPTY_PAGE_MESSAGE_H1), 6000).getText();
    }

    async isProceedCheckoutButtonEnabled() {
        return await this.driver.wait(until.elementLocated(this.PROCEED_CHECKOUT_BTN), 6000).isEnabled();
    }

    async getAllertText() {
        return (await this.driver.findElement(this.ALLERT_TEXT).getText()).trim();
    }

    // Single Article

    async setArticleQuantityById(productId: string, quantity: number) {
        await this.driver.wait(until.elementLocated(this.ARTICLE_QUANTITY_BY_ID(productId)), 6000).clear();
        await this.driver.findElement(this.ARTICLE_QUANTITY_BY_ID(productId)).sendKeys(quantity);
    }

    async setArticleQuantityByName(productName: string, quantity: number) {
        await this.driver.wait(until.elementLocated(this.ARTICLE_QUANTITY_BY_NAME(productName)), 6000).clear();
        await this.driver.findElement(this.ARTICLE_QUANTITY_BY_NAME(productName)).sendKeys(quantity);
    }

    async getProductNameById(productId: string) {
        return await this.driver.wait(until.elementLocated(this.ARTICLE_NAME_BY_ID(productId)), 5000).getText();
    }

    async getProductSinglePriceById(productId: string) {
        return await this.driver.wait(until.elementLocated(this.ARTICLE_PRICE_BY_ID(productId)), 5000).getText();
    }

    async getProductQuantityById(productId: string) {
        return await this.driver.findElement(this.ARTICLE_QUANTITY_BY_ID(productId)).getAttribute("value");
    }

    async getProductSubtotalById(productId: string) {
        return await this.driver.findElement(this.ARTICLE_SUBTOTAL_BY_ID(productId)).getText();
    }

    async getProductSinglePriceByName(productName: string) {
        return await this.driver.wait(until.elementLocated(this.ARTICLE_PRICE_BY_NAME(productName)), 5000).getText();
    }

    async getProductQuantityByName(productName: string) {
        return await this.driver.findElement(this.ARTICLE_QUANTITY_BY_NAME(productName)).getAttribute("value");
    }

    async getProductSubtotalByName(productName: string) {
        return await this.driver.findElement(this.ARTICLE_SUBTOTAL_BY_NAME(productName)).getText();
    }

    async getProductInfoByName(productName: string): Promise<ICartProductInfo> {
        let productInto: ICartProductInfo;
        productInto.name = productName;
        productInto.price = await this.getProductSinglePriceByName(productName);
        productInto.quantity = +(await this.getProductQuantityByName(productName));
        productInto.productTotal = await this.getProductSubtotalByName(productName);
        return productInto;
    }

    async getProductInfoById(productId: string): Promise<ICartProductInfo> {
        let productInto: ICartProductInfo = {
            name: await this.getProductNameById(productId),
            price: await this.getProductSinglePriceById(productId),
            quantity: +(await this.getProductQuantityById(productId)),
            productTotal: await this.getProductSubtotalById(productId),
        };
        return productInto;
    }

    // Articles collection

    async getProductNames() {
        const collection: WebElement[] = await this.driver.findElements(this.ARTICLE_NAMES);
        const names = await Promise.all(collection.map(async (element) => await element.getText()));
        return names;
    }

    async getProductWithMaxPrice(products: ICartProductInfo[]): Promise<ICartProductInfo> {
        let maxPriceProduct: ICartProductInfo = products[0];
        products.forEach((product, index) => {
            if (moneyToNumber(product.price) > moneyToNumber(maxPriceProduct.price)) {
                maxPriceProduct = products[index];
            }
        })
        return maxPriceProduct;
    }

    async getProductWithMinPrice(products: ICartProductInfo[]): Promise<ICartProductInfo> {
        let minPriceProduct: ICartProductInfo = products[0];
        products.forEach((product, index) => {
            if (moneyToNumber(product.price) < moneyToNumber(minPriceProduct.price)) {
                minPriceProduct = products[index];
            }
        })
        return minPriceProduct;
    }

    // Cart attrs

    async getCartSubtotal() {
        return await this.driver.findElement(this.CART_SUBTOTAL).getText();
    }

    async getCartTotal() {
        return await this.driver.findElement(this.CART_TOTAL).getText();
    }

    async setShippingCountryAndPostCode(country, postcode) {
        await this.driver.wait(until.elementLocated(this.SHIPPING_COUNTRY_NAME), 5000).click();
        await this.driver.wait(until.elementLocated(this.SHIPPING_DATA_POPUP), 5000);
        await this.driver.findElement(this.SHIPPING_COUNTRY_DROPDOWN).click();
        await this.driver.wait(until.elementLocated(this.SHIPPING_COUNTRY_OPTIONS(country)), 5000).click();
        await this.driver.findElement(this.POSTCODE_INPUT).sendKeys(postcode);
        await this.driver.findElement(this.UPDATE_SHIPPING_FEES).click();
    }

    async getShippingCountryName() {
        return (await this.driver.findElement(this.SHIPPING_COUNTRY_NAME).getText()).trim();
    }

    async getShippingCost() {
        await driver.sleep(3000);
        return (await this.driver.wait(until.elementLocated(this.SHIPPING_COST), 5000).getText()).trim();
    }
}