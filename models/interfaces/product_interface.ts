import { WebElement } from "selenium-webdriver";

export interface IProduct {
    addProductToBasket(productIdentificator: string, quantity?: number);

    getPrice(productIdentificator: string);

    getName(productIdentificator: string);

    addToCart(productIdentificator: string);
}

export interface ICartProductInfo {
    name: string;
    price: string;
    quantity: number;
    productTotal: string;
} 

export interface ICatalogProductInfo {
    name: string;
    price: string;
    quantity: number;
} 