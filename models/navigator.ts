import config from 'config';
import { WebDriver } from 'selenium-webdriver';
import driver from '../base/factory';

export class Navigator {
    private driver: WebDriver;

    constructor() {
        this.driver = driver;
    }

    async navigateTo(path) {
       return await this.driver.get(`${config.get("baseUrl")}${path}`);
    }

    async getUrl() {
        return await this.driver.getCurrentUrl();
    }
}