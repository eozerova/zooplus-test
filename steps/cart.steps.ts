const { Given, When, Then } = require('@cucumber/cucumber');
import driver from '../base/factory';
import { Navigator } from '../models/navigator';
import { Products } from '../models/components/products';
import { GeneralPage } from '../models/pages/general.page';
import { CartPage } from '../models/pages/cart.page';
import ShareData from '../base/memory';
import { ICartProductInfo, ICatalogProductInfo } from '../models/interfaces/product_interface';
import { numberToMoney, moneyToNumber } from '../support/utils';
const expect = require('chai').expect;

const navigation = new Navigator();
const product = new Products();
const general = new GeneralPage();
const cart = new CartPage();

Given('User opens {string}', async (path) => {
    await navigation.navigateTo(path);
});

Given('Adds the product {string} with {float} quantity through catalog', async (productId: string, quantity: number) => {
    const productInfo: ICatalogProductInfo = await product.addProductToBasket(productId, quantity);
    ShareData.save("catProductInfo", productInfo);
});

When('User goes to Cart', { timeout: 10000 }, async () => {
    await general.goToBasket();
    const url = await navigation.getUrl();
    expect(url).to.contain('/cart');
});

When('Adds the product from recommendation', async () => {
    const productData: ICatalogProductInfo = await cart.recommendations.addProductToBasket();
    await driver.sleep(2000);
    ShareData.save("recomProductData", productData);
});

When('Adds several products {string} with {float} quantity through catalog', async (productIds: string, quantity: number) => {
    const ids = productIds.split(",");
    const productsData: ICatalogProductInfo[] = [];
    for (const id of ids) {
        const data = await product.addProductToBasket(id, quantity);
        productsData.push(data);
    }

    ShareData.save("productsData", productsData);
});

When('Removes {string} product', async (productId: string) => {
    await cart.setArticleQuantityById(productId, 0);
});

When('Increments by one the most cheap product', async () => {
    const minPriceProduct = await cart.getProductWithMinPrice(ShareData.get("productsData"));
    minPriceProduct.quantity = minPriceProduct.quantity + 1;
    await cart.setArticleQuantityByName(minPriceProduct.name, minPriceProduct.quantity);
});

When('User removes the most expensive product', async () => {
    const maxPriceProduct = await cart.getProductWithMaxPrice(ShareData.get("productsData"));
    maxPriceProduct.quantity = 0;
    await cart.setArticleQuantityByName(maxPriceProduct.name, 0);
});

When('Sets shipping country {string} and postcode {float}', async (country: string, postcode: number) => {
    await cart.setShippingCountryAndPostCode(country, postcode);
});

Then('User should see the added product {string} with {float} quantity', async (productId: string, expectedQuality: number) => {
    const { price: actualPrice, quantity: actualQuantity, productTotal: actualSubtotal } = await cart.getProductInfoById(productId);

    const expectedSubtotal = numberToMoney(moneyToNumber(actualPrice) * actualQuantity);
    const expectedInfo: ICatalogProductInfo = ShareData.get("catProductInfo");

    expect(actualPrice).to.be.equal(expectedInfo.price);
    expect(actualQuantity).to.be.equal(expectedQuality);
    expect(actualSubtotal).to.be.equal(expectedSubtotal);

    ShareData.save("expectedSubtotal", expectedSubtotal);
});

Then('Total and subtotal should be correct including shipping fees {float}', async (delivery: number) => {
    const actualCartSubtotal = await cart.getCartSubtotal();
    const actualCartTotal = await cart.getCartTotal();

    const expectedTotal = numberToMoney(moneyToNumber(actualCartSubtotal) + delivery);

    expect(actualCartSubtotal).to.be.equal(ShareData.get("expectedSubtotal"));
    expect(actualCartTotal).to.be.equal(expectedTotal);
});

Then('Total and subtotal should include both products and shipping fees {float}', async (delivery: number) => {
    const actualCartSubtotal = await cart.getCartSubtotal();
    const actualCartTotal = await cart.getCartTotal();

    const recomProductData: ICatalogProductInfo = ShareData.get("recomProductData");
    const catProductInfo: ICatalogProductInfo = ShareData.get("catProductInfo");

    const expectedSubtotal = (moneyToNumber(recomProductData.price) * recomProductData.quantity) + (moneyToNumber(catProductInfo.price) * catProductInfo.quantity);
    const expectedTotal = expectedSubtotal + delivery;

    expect(actualCartSubtotal).to.be.equal(numberToMoney(expectedSubtotal));
    expect(actualCartTotal).to.be.equal(numberToMoney(expectedTotal));
})

Then('Products should be added', async () => {
    const addedProductNames = await cart.getProductNames();
    const expectedProductInfos: ICatalogProductInfo[] = [ShareData.get("recomProductData"), ShareData.get("catProductInfo")];
    const expectedNames = expectedProductInfos.map((item) => item.name);

    expect(addedProductNames.sort()).to.be.deep.equal(expectedNames.sort());
});

Then('The {string} message should be displayed', async (message: string) => {
    const actualMessage = await cart.getEmptyPageMessage();
    expect(actualMessage).to.be.equal(message);
});

Then('Cart subtotal and total amounts should be updated', async () => {
    const products: ICartProductInfo[] = ShareData.get("productsData");
    const delivery = await cart.getShippingCost();

    let expectedSubtotal: number = 0;
    products.forEach((product) => {
        expectedSubtotal = expectedSubtotal + moneyToNumber(product.price) * product.quantity;
    })

    const expectedTotal = expectedSubtotal + moneyToNumber(delivery);

    const actualCartSubtotal = await cart.getCartSubtotal();
    const actualCartTotal = await cart.getCartTotal();

    expect(actualCartSubtotal).to.be.equal(numberToMoney(expectedSubtotal));
    expect(actualCartTotal).to.be.equal(numberToMoney(expectedTotal));
});


Then('Shipping fees should be changed to {float}', async (expectedFees: number) => {
    const actualFees = await cart.getShippingCost();
    expect(actualFees).to.be.equal(`€${expectedFees}`);
});

Then('Country {string} and postcode {int} should be displayed', async (country: string, postcode: number) => {
    const actualCountry = await cart.getShippingCountryName();
    expect(actualCountry).to.be.equal(`${country} (${postcode})`);
});

Then('Total amount should be updated by new shipping fees {float}', async (shippingFees: number) => {
    const productInfo: ICartProductInfo = ShareData.get("catProductInfo");
    const subTotal = moneyToNumber(productInfo.price) * productInfo.quantity;
    const actualTotal = await cart.getCartTotal();
    expect(actualTotal).to.be.equal(numberToMoney(subTotal + shippingFees));
});

Then('{string} button should be disabled', async (string) => {
    const state = await cart.isProceedCheckoutButtonEnabled();

    expect(state).to.be.false;
});

Then('Warning message {string} should be displayed', async (expectedMessage: string) => {
    const actualMessage = await cart.getAllertText();
    expect(actualMessage).to.be.equal(expectedMessage);
});